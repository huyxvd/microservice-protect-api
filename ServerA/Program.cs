﻿WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddHttpClient("callServerB", c =>
{
    c.BaseAddress = new Uri("http://localhost:5001");
    // Cấu hình thêm các tùy chọn khác nếu cần
    c.DefaultRequestHeaders.Add("x-api-key", "your-api-key-value"); // Gửi kèm API key
});


WebApplication app = builder.Build();

app.MapGet("/a", async (IHttpClientFactory _httpClientFactory) =>
{
    // Sử dụng HttpClient được tạo và cấu hình trước đó
    HttpClient httpClient = _httpClientFactory.CreateClient("callServerB");

    HttpResponseMessage response = await httpClient.GetAsync("/b"); // gửi request vào http://localhost:5001/b

    // Xử lý kết quả và trả về
    if (response.IsSuccessStatusCode)
    {
        string content = await response.Content.ReadAsStringAsync();

        return $"server B bảo là {content}";
    }

    return "không gọi qua server B được";
});

app.Run();
