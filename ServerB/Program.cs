﻿using Microsoft.AspNetCore.Mvc;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

// Add services to the container.

WebApplication app = builder.Build();

// Configure the HTTP request pipeline.
app.UseMiddleware<ApiKeyMiddleware>(); // register api

app.MapGet("/b", () =>
{
    return "Hello World!";
});

app.Run();

public class ApiKeyMiddleware
{
    private readonly RequestDelegate _next;

    public ApiKeyMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        if (!context.Request.Headers.TryGetValue("x-api-key", out var apiKeyValue))
        {
            context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            await context.Response.WriteAsync("API key is missing.");
            return;
        }

        // Kiểm tra api key có hợp lệ hay không. Bạn hoàn toàn có thể kiểm tra bằng cách truy vấn vào database
        if (apiKeyValue != "your-api-key-value")
        {
            context.Response.StatusCode = StatusCodes.Status403Forbidden;
            await context.Response.WriteAsync("Invalid API key.");
            return;
        }

        await _next.Invoke(context); // x-api-key hợp lệ
    }
}